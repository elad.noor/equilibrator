# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2020 Institute for Molecular Systems Biology,
# ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import argparse
import datetime
import logging
import os
import sys
import time
import django
from numpy import floor

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "equilibrator.settings")
django.setup()

from util import database_io

logging.getLogger().setLevel(logging.INFO)

############################ MAIN #############################
parser = argparse.ArgumentParser(
    description=('Create download files for eQuilibrator')
)
parser.add_argument('--noinput', action='store_true',
                    help='Disable user prompt messages')
args = parser.parse_args()

logging.info(f"Welcome to the {__file__} script. "
    "This script generates the .csv and .json download files for the website.")
logging.info(" **** NOTE: the whole process can take a couple of hours ****")
if not args.noinput:
    s = input("Are you sure you want to proceed [y/N]? ")
    if s.lower() not in ["y", "yes"]:
        sys.exit(0)

start = time.time()

logging.info('> Exporting database to JSON and CSV files')
raise NotImplementedError("use `equilibrator-api` directly to generate these"
                          "files (independently of `equilibrator-web`)")

end = time.time()
elapsed = datetime.timedelta(seconds=floor(end - start))
logging.info('Elapsed loading time = %s' % str(elapsed))

