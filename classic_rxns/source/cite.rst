Relevant publications
=====================

#. M.E. Beber, M.G. Gollub, D. Mozaffari, K.M. Shebek, A.I. Flamholz, R. Milo, E. Noor (2021) **eQuilibrator 3.0: a database solution for thermodynamic constant estimation** *Nucleic Acids Research* [`DOI: 10.1093/nar/gkab1106 <https://doi.org/10.1093/nar/gkab1106>`_]
#. A.I. Flamholz, E. Noor, A. Bar-Even, R. Milo (2012) **eQuilibrator - the biochemical thermodynamics calculator** *Nucleic Acids Research* [`DOI: 10.1093/nar/gkr874 <https://doi.org/10.1093/nar/gkr874>`_]
#. E. Noor, A. Bar-Even, A.I. Flamholz, Y. Lubling, D. Davidi, R. Milo (2012) **An integrated open framework for thermodynamics of reactions that combines accuracy and coverage** *Bioinformatics 28:2037-2044* [`DOI: 10.1093/bioinformatics/bts317 <https://doi.org/10.1093/bioinformatics/bts317>`_]
#. E. Noor, H.S. Haraldsdóttir, R. Milo, R.M.T. Fleming (2013) **Consistent Estimation of Gibbs Energy Using Component Contributions** *PLoS Comput Biol 9(7): e1003098* [`DOI: 10.1371/journal.pcbi.1003098 <https://doi.org/10.1371/journal.pcbi.1003098>`_]
#. E. Noor, A. Bar-Even, A.I. Flamholz, E. Reznik, W. Liebermeister, R. Milo (2014) **Pathway Thermodynamics Highlights Kinetic Obstacles in Central Metabolism** *PLoS Comput Biol 10(2):e1003483* [`DOI: 10.1371/journal.pcbi.1003483 <https://doi.org/10.1371/journal.pcbi.1003483>`_]
