[project]
name = "equilibrator-web"
description = "A web service for fetching standard thermodynamic potentials of biochemical reactions"
readme = {file = "README.md", content-type = "text/markdown"}
license = {text = "MIT License"}
authors = [
  {name = "Elad Noor", email = "elad.noor@weizmann.ac.il"},
  {name = "Moritz E. Beber", email = "midnighter@posteo.net"}
]
keywords = [
    "component contribution",
    "Gibbs energy",
    "biochemical reaction",
    "eQuilibrator"
]
classifiers = [
    "Development Status :: 4 - Beta",
    "Intended Audience :: Science/Research",
    "Topic :: Scientific/Engineering :: Bio-Informatics",
    "Topic :: Scientific/Engineering :: Chemistry",
    "License :: OSI Approved :: MIT License",
    "Natural Language :: English",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Programming Language :: Python :: 3.12",
]
dynamic = ["version"]
requires-python = ">=3.9"
dependencies = [
    "seaborn~=0.13",
    "jupyter~=1.0",
    "bokeh~=3.3",
    "sphinx_rtd_theme~=2.0",
    "gunicorn~=21.2",
    "stopit~=1.1",
    "psycopg2-binary~=2.9",
    "nltk~=3.8",
    "pyparsing~=3.1",
    "django~=3.2.24",
    "django-haystack[elasticsearch]==3.2.1",
    "django-utils-six~=2.0",
    "django-extensions~=3.2.3",
    "django-cookies-samesite~=0.9",
    "django-generate-secret-key~=1.0",
    "equilibrator-cache==0.6.0",
    "component-contribution==0.6.0",
    "equilibrator-api==0.6.0",
    "equilibrator-pathway==0.6.0",
    "sbtab==1.0.8",
]

[project.optional-dependencies]
test = [
    "pytest",
    "pytest-cov",
    "pytest-raises",
    "pytest-mock",
    "pytest-allclose",
]
development = [
    "black",
    "isort",
    "flake8",
    "safety",
    "tox",
    "twine"
]
deployment = [
    "click",
    "click-log",
    "python-dateutil",
]

[project.urls]
repository = "https://gitlab.com/equilibrator/equilibrator-web/"
documentation = "https://equilibrator.readthedocs.io/en/latest/"

[build-system]
requires = [ "setuptools>=41", "wheel", "setuptools-git-versioning<2", ]
build-backend = "setuptools.build_meta"

[tool.distutils.bdist_wheel]
universal = true

[tool.setuptools]
zip-safe = true

[tool.setuptools.packages.find]
where = ["equilibrator", "gibbs", "matching", "pathway"]

[tool.setuptools-git-versioning]
enabled = true

[tool.black]
exclude = '''
(
    __init__.py
)
'''

[tool.ruff]
target-version = "py312"
line-length = 88

[tool.ruff.lint]
ignore = ["F401"]
