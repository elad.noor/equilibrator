# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2020 Institute for Molecular Systems Biology,
# ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from django import forms
from util import constants


class BuildPathwayModelForm(forms.Form):
    pathway_file = forms.FileField(required=True)
    default_lb_value = forms.FloatField(required=True)
    default_lb_unit = forms.MultipleChoiceField(
        required=True, choices=constants.ABUNDANCE_UNITS_CHOICES
    )
    default_ub_value = forms.FloatField(required=True)
    default_ub_unit = forms.MultipleChoiceField(
        required=True, choices=constants.ABUNDANCE_UNITS_CHOICES
    )
    p_h = forms.FloatField(required=True)
    p_mg = forms.FloatField(required=True)
    ionic_strength = forms.FloatField(required=True)
    temperature = forms.FloatField(required=False)
    e_potential = forms.FloatField(required=False)
    mdf_ecm_toggle = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices=[("MDF", "MDF"), ("ECM", "ECM")],
    )

    def GetOptimizationMethod(self) -> str:
        return self.cleaned_data["mdf_ecm_toggle"]

class AnalyzePathwayModelForm(forms.Form):
    pathway_file = forms.FileField(required=True)
    html_sbtab_toggle = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices=[("HTML", "HTML"), ("SBtab", "SBtab")],
    )

    def GetOutputFormat(self) -> str:
        return self.cleaned_data["html_sbtab_toggle"]
