# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2020 Institute for Molecular Systems Biology,
# ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import logging
import os
from io import StringIO
from typing import Tuple

import pandas as pd
import stopit
from django import forms
from django.core.files.uploadedfile import UploadedFile
from django.http import HttpResponse, HttpResponsePermanentRedirect
from django.shortcuts import render
from django.template.context_processors import csrf
from equilibrator import settings
from equilibrator_api import Q_, ComponentContribution
from equilibrator_api.model.bounds import Bounds
from equilibrator_pathway import EnzymeCostModel, Pathway, ThermodynamicModel
from gibbs import _ccache, _predictor
from gibbs.conditions import AqueousParams
from matplotlib import pyplot as plt
from psycopg2 import DatabaseError as DatabaseTimoutError
from sbtab.SBtab import SBtabDocument, SBtabError, SBtabTable

from .forms import AnalyzePathwayModelForm, BuildPathwayModelForm


def _load_sbtab(pathway_file: UploadedFile) -> SBtabDocument:
    """Load the contents of the uploaded file into an SBtabDocument."""

    # We are forced to add 'application/octet-stream' as an option since Windows
    # doesn't recognize the example files as text and therefore wouldn't work.
    if pathway_file.content_type not in [
        "text/csv",
        "text/tab-separated-values",
        "application/octet-stream",
    ]:
        raise Exception(
            f"Uploaded file extension must be .csv or .tsv, not {pathway_file.content_type}"
        )
    try:
        sbtabdoc = SBtabDocument(
            "pathway", pathway_file.read().decode(encoding="utf-8"), pathway_file.name
        )
    except SBtabError as e:
        raise ValueError(str(e))
    return sbtabdoc


def _build_pathway_model(
    form: forms.Form, sbtabdoc: SBtabDocument
) -> Tuple[Pathway, str, AqueousParams]:
    aq_params = AqueousParams.initialize(form=form)
    optimization_method = form.GetOptimizationMethod()

    comp_contrib = ComponentContribution(ccache=_ccache, predictor=_predictor)
    comp_contrib.p_h = aq_params.p_h
    comp_contrib.p_mg = aq_params.p_mg
    comp_contrib.ionic_strength = aq_params.ionic_strength
    comp_contrib.temperature = aq_params.temperature

    default_bounds = {}
    for l in ["lb", "ub"]:
        val = form.cleaned_data.get(f"default_{l}_value")
        unit = form.cleaned_data.get(f"default_{l}_unit")
        if type(unit) == list:
            unit = unit[0]
        default_bounds[l] = Q_(float(val), unit)
        assert default_bounds[l].check("[concentration]")

    bounds = Bounds.get_default_bounds(comp_contrib)
    bounds.default_lb = default_bounds["lb"]
    bounds.default_ub = default_bounds["ub"]

    pathway = Pathway.from_network_sbtab(
        sbtabdoc, comp_contrib=comp_contrib, freetext=True, bounds=bounds
    )
    pathway.config_dict["algorithm"] = form.GetOptimizationMethod()
    pathway.config_dict["stdev_factor"] = "1.96"

    fname_base, ext = os.path.splitext(sbtabdoc.filename)
    output_fname = f"{fname_base}_{optimization_method}.tsv"

    return pathway, output_fname, aq_params


def _format_concentration(c_in_molar: float) -> str:
    """Format the concentration value as a human readable string."""
    if c_in_molar > 1e-1:
        return "{:,.2f} M".format(c_in_molar)
    elif c_in_molar > 1e-4:
        return "{:,.2f} mM".format(c_in_molar * 1e3)
    elif c_in_molar > 1e-7:
        return "{:,.2f} μM".format(c_in_molar * 1e6)
    elif c_in_molar > 1e-10:
        return "{:,.2f} nM".format(c_in_molar * 1e9)
    else:
        return str(c_in_molar)


def DefinePathwayPage(request):
    """Renders the landing page."""
    template_data = csrf(request)
    aq_params = AqueousParams.initialize(form=None, cookies=request.COOKIES)
    template_data["aq_params"] = aq_params
    response = render(request, "define_pathway_page.html", template_data)
    return response


def BuildPathwayModel(request):
    """Renders a page for a particular reaction."""
    try:
        form = BuildPathwayModelForm(request.POST, request.FILES)

        if not form.is_valid():
            raise Exception(str(form.errors))

        sbtabdoc = _load_sbtab(request.FILES["pathway_file"])

        try:
            thread_timeout = settings.PATHWAYS["csv_convert_thread_timeout"]
            logging.error("Timeout in %d seconds" % thread_timeout)
            with stopit.ThreadingTimeout(thread_timeout) as to_ctx_mgr:
                assert to_ctx_mgr.state == to_ctx_mgr.EXECUTING
                pathway, output_fname, aq_params = _build_pathway_model(form, sbtabdoc)
                logging.info(output_fname)

        # sometimes when the time runs out in the middle of an SQL query, we
        # get this exception. The response should be the same as if the
        # to_ctx_mgr indicates a timeout
        except DatabaseTimoutError:
            return render(request, "pathway_timeout.html", {})

        # There was a timeout event, i.e. it took too much time to parse
        # the reactions in the CSV file
        if not to_ctx_mgr:
            return render(request, "pathway_timeout.html", {})

        response = HttpResponse(content_type="text/tab-separated-values")
        response["Content-Disposition"] = 'attachment; filename="%s"' % output_fname

        if form.GetOptimizationMethod() == "MDF":
            sbtabdoc = pathway.to_sbtab()
        elif form.GetOptimizationMethod() == "ECM":
            pathway.config_dict.update(
                {
                    "version": "3",
                    "kcat_source": "gmean",
                    "denominator": "CM",
                    "regularization": "volume",
                }
            )
            sbtabdoc = pathway.to_sbtab()

            # add the Parameter table (using default values)
            pdata = []
            _compound_to_id = {
                cpd: cid for cid, cpd in pathway.compound_dict.items()
            }
            for rxn in pathway.reactions:
                rid = rxn.rid
                pdata.append(
                    ("catalytic rate constant geometric mean", rid, "", "1", "1/s")
                )
                pdata.append(("protein molecular mass", rid, "", "1", "Da"))
                for cid, cpd in pathway.compound_dict.items():
                    if rxn.get_coeff(cpd) != 0:
                        pdata.append(("Michaelis constant", rid, cid, "1", "mM"))
            for cid in pathway.compound_ids:
                pdata.append(("molecular mass", "", cid, f"{cpd.mass:.3f}", "Da"))

            param_df = pd.DataFrame(
                data=pdata,
                columns=["!QuantityType", "!Reaction", "!Compound", "!Value", "!Unit"],
            ).sort_values(["!QuantityType", "!Reaction", "!Compound"])

            param_sbtab = SBtabTable.from_data_frame(
                df=param_df, table_id="Parameter", table_type="Quantity"
            )
            sbtabdoc.add_sbtab(param_sbtab)
        else:
            raise Exception("Internal error, the only options are MDF or ECM.")

        response.write(sbtabdoc.to_str())
        aq_params.SetCookies(response)
        return response

    except Exception as e:
        raise e
        logging.error(e)
        template_data = {"pathway": None, "mdf_result": None, "error_message": str(e)}
        return render(request, "pathway_result_page.html", template_data)


def PathwayResultPage(request):
    """Renders a page for a particular reaction."""

    pathway = None

    try:
        form = AnalyzePathwayModelForm(request.POST, request.FILES)

        if "pathway_file" not in request.FILES:
            raise ValueError("No file selected.")

        if not form.is_valid():
            raise ValueError("Invalid pathway form.")

        comp_contrib = ComponentContribution(ccache=_ccache, predictor=_predictor)
        sbtabdoc = _load_sbtab(request.FILES["pathway_file"])

        fname_base, ext = os.path.splitext(sbtabdoc.filename)
        output_fname = f"{fname_base}_report.tsv"

        thread_timeout = settings.PATHWAYS["pathway_analysis_thread_timeout"]
        with stopit.ThreadingTimeout(thread_timeout) as to_ctx_mgr:
            assert to_ctx_mgr.state == to_ctx_mgr.EXECUTING

            _config_sbtab = sbtabdoc.get_sbtab_by_id("Configuration")
            if _config_sbtab is None:
                raise ValueError("Missing 'Configuration' table")
            try:
                config_df = _config_sbtab.to_data_frame().set_index("Option")
                optimization_method = config_df.Value["algorithm"]
            except AttributeError as e:
                raise ValueError(str(e))
            except KeyError as e:
                raise ValueError(str(e))

            logging.info("Parsed pathway: %s" % optimization_method)

            if optimization_method == "MDF":
                pathway = ThermodynamicModel.from_sbtab(
                    sbtabdoc, comp_contrib=comp_contrib
                )
                if pathway.Nr == 0:
                    raise ValueError("Empty pathway")
                logging.info("Starting pathway analysis")
                analysis_results = pathway.mdf_analysis()
            elif optimization_method == "ECM":
                ecm_model = EnzymeCostModel.from_sbtab(
                    sbtabdoc, comp_contrib=comp_contrib
                )
                pathway = ecm_model._thermo_model
                if pathway.Nr == 0:
                    raise ValueError("Empty pathway")
                analysis_results = ecm_model.optimize_ecm()
            else:
                raise ValueError(
                    "Input SBtab file does not qualify as MDF nor ECM model"
                )
            logging.info("Finished pathway analysis")

        if not to_ctx_mgr:
            return HttpResponsePermanentRedirect("/timeout")

        logging.info(
            "Calculated %s score: %s" % (optimization_method, analysis_results.score)
        )

        if form.GetOutputFormat() == "SBtab":
            response = HttpResponse(content_type="text/tab-separated-values")
            response["Content-Disposition"] = 'attachment; filename="%s"' % output_fname
            response.write(analysis_results.to_sbtab().to_str())
        elif form.GetOutputFormat() == "HTML":
            # format the reaction table
            reaction_table = analysis_results.reaction_df.copy()
            reaction_table["flux"] = reaction_table.flux.map(
                lambda g: "{:,.3g}".format(g.m_as(""))
            )

            dg_to_str = lambda g: "{:,.1f} kJ/mol".format(g.m_as("kJ/mol"))
            reaction_table["standard_dg_prime"] = reaction_table.standard_dg_prime.map(
                dg_to_str
            )
            reaction_table[
                "physiological_dg_prime"
            ] = reaction_table.physiological_dg_prime.map(dg_to_str)
            reaction_table[
                "optimized_dg_prime"
            ] = reaction_table.optimized_dg_prime.map(dg_to_str)

            # format the compound table
            compound_table = analysis_results.compound_df.copy()
            compound_table = compound_table.loc[compound_table.compound_id != "H2O", :]
            compound_table["concentration"] = compound_table.concentration_in_molar.map(
                _format_concentration
            )
            compound_table["lower_bound"] = compound_table.lower_bound_in_molar.map(
                _format_concentration
            )
            compound_table["upper_bound"] = compound_table.lower_bound_in_molar.map(
                _format_concentration
            )

            template_data = {
                "optimization_method": optimization_method,
                "pathway": pathway,
                "analysis_results": analysis_results,
                "reaction_table": list(reaction_table.itertuples()),
                "compound_table": list(compound_table.itertuples()),
            }

            svg_data = StringIO()
            if optimization_method == "MDF":
                fig1, ax1 = plt.subplots(
                    1,
                    1,
                    figsize=(8, 2 + 0.2 * analysis_results.compound_df.shape[0]),
                    dpi=90,
                )
                analysis_results.plot_concentrations(ax1)
                fig1.tight_layout()
                fig1.savefig(svg_data, format="svg")
                fig2, ax2 = plt.subplots(1, 1, figsize=(8, 6), dpi=90)
                analysis_results.plot_driving_forces(ax2)
                fig2.tight_layout()
                fig2.savefig(svg_data, format="svg")
            elif optimization_method == "ECM":
                fig1, ax1 = plt.subplots(1, 1, figsize=(8, 6), dpi=90)
                analysis_results.plot_thermodynamic_profile(ax1)
                fig1.tight_layout()
                fig1.savefig(svg_data, format="svg")
                fig2, ax2 = plt.subplots(1, 1, figsize=(8, 6), dpi=90)
                analysis_results.plot_enzyme_demand_breakdown(ax2, plot_measured=False)
                fig2.tight_layout()
                fig2.savefig(svg_data, format="svg")
            else:
                raise ValueError(
                    "Input SBtab file does not qualify as MDF nor ECM model"
                )
            template_data.update({"figure_svg": svg_data.getvalue()})

            response = render(request, "pathway_result_page.html", template_data)
        else:
            raise ValueError("unknown output format: " + form.GetOutputFormat())

        return response

    except ValueError as e:
        logging.error(e)
        return render(
            request,
            "pathway_result_page.html",
            {
                "error_message": str(e),
            },
        )
