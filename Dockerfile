FROM python:3.12-slim-bullseye

MAINTAINER elad.noor@weizmann.ac.il

WORKDIR /app/src

COPY . ./

RUN set -eux \
    && apt-get update \
    && apt-get install --yes \
        build-essential \
        postgresql-client \
        pgloader \
        openbabel \
        cmake \
    && pip install -U pip wheel \
    && pip install -e . \
    && rm -rf /root/.cache/pip \
    && apt-get purge --yes \
        build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN ["python", "-c", "from equilibrator_api import ComponentContribution; cc = ComponentContribution()"]
#CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

CMD ["gunicorn", "--config", "config/gunicorn.conf.py", "equilibrator.wsgi:application"]

EXPOSE 8000
