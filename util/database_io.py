# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2020 Institute for Molecular Systems Biology,
# ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from typing import Union, Tuple
import gzip
import json
import logging
from tqdm import tqdm
from pkg_resources import resource_filename, resource_string

import pandas as pd
from django.apps import apps
from django.db.utils import DataError
import equilibrator_api
from equilibrator_cache import CompoundCache


ADDITIONAL_DATA_JSON = json.loads(
    resource_string(__name__, '../data/additional_compound_data.json')
)
CITATION_DATA_JSON = json.loads(
    resource_string(__name__, '../data/citation_data.json')
)
COMPOUND_NAMES_DF = pd.read_csv(
    resource_filename(__name__, '../data/compound_names.csv'), index_col=None
)
COMPOUND_RENAMING_DF = pd.read_csv(
    resource_filename(__name__, '../data/compound_renaming.csv'), index_col=None
)
KEGG_REACTIONS_JSON = json.load(
    gzip.open(resource_filename(__name__, '../data/kegg_reactions.json.gz'))
)
KEGG_ENZYMES_JSON = json.load(
    gzip.open(resource_filename(__name__, '../data/kegg_enzymes.json.gz'))
)

# Cache compounds so we can look them up faster.
CITATIONS_CACHE = {}


def load_citation_data():
    values_source_model = apps.get_model('gibbs.ValueSource')
    values_source_model.objects.all().delete()

    for cd in CITATION_DATA_JSON:
        data = json.dumps(cd)
        name = cd['name']
        source, created = values_source_model.objects.get_or_create(
                name=name, data=data)
        source.save()


def GetSource(source_string: str) -> Union[str, None]:
    if not source_string:
        return None

    source_model = apps.get_model('gibbs.ValueSource')
    lsource = source_string.strip().lower()
    if lsource in CITATIONS_CACHE:
        return CITATIONS_CACHE[lsource]

    try:
        source = source_model.objects.get(name__iexact=lsource)
        CITATIONS_CACHE[lsource] = source
        return source
    except source_model.DoesNotExist:
        logging.warning('Source "%s" does not exist in the database',
                        source_string)
        return None


def get_compound_names() -> pd.DataFrame:

    names_df = COMPOUND_NAMES_DF.copy()
    names_df["synonyms"] = names_df.synonyms.str.split('|')
    names_df.set_index("compound_id", inplace=True)

    to_delete = []
    for row in COMPOUND_RENAMING_DF.itertuples(index=False):
        if row.command == 'remove':
            # remove 'name' from the list of names
            try:
                names_df.at[row.compound_id, "synonyms"].remove(row.name)
            except ValueError:
                logging.warning(
                    f"The name {row.name} is not one of the options for "
                    f"{row.compound_id}, so it cannot be removed"
                )
        elif row.command == 'add':
            # put 'name' at the end of the list if it doesn't exist already
            if row.name not in names_df.at[row.compound_id, "synonyms"]:
                names_df.at[row.compound_id, "synonyms"].append(row.name)
        elif row.command == 'delete':
            names_df.drop(row.compound_id, axis=0)
        elif row.command == 'replace':
            # move the synonyms from the 'old' compound to the 'new' one
            old_synonyms = names_df.at[row.compound_id, "synonyms"]
            new_synonyms = names_df.at[row.name, "synonyms"]
            new_synonyms += list(set(old_synonyms).difference(new_synonyms))
            names_df.drop(row.compound_id, axis=0)
        else:
            raise ValueError(f"Unknown command: {row.command}")

    names_df.drop(to_delete, axis=0)

    # add the KEGG ID itself as one of the names, this enables us to
    # write down reactions using KEGG IDs in the search bar
    for row in names_df.itertuples(index=True):
        row.synonyms.append(row.Index.replace("kegg:", ""))

    return names_df.reset_index().sort_values("compound_id", axis=0)


def get_or_create_compound(
        ccache: CompoundCache,
        compound_id: str
) -> Tuple["Compound", bool]:
    compound_model = apps.get_model('gibbs.Compound')
    eqapi_compound = ccache.get_compound(compound_id)
    if eqapi_compound is None:
        logging.debug(
            f"Cannot find {compound_id} in equilibrator-cache"
        )
        return None, False

    compound, created = compound_model.objects.get_or_create(
        eqapi_id=eqapi_compound.id
    )
    if created:
        compound.formula = eqapi_compound.formula
        compound.inchi = eqapi_compound.inchi
        compound.mass = eqapi_compound.mass

    return compound, created


def load_kegg_compound_names(ccache: CompoundCache) -> None:
    # TODO: instead of relying on raw TSV files, use the cache itself to
    #  find the relevant names and IDs

    names_df = get_compound_names()
    cname_model = apps.get_model('gibbs.CommonName')
    for row in tqdm(
        names_df.itertuples(index=False),
        total=names_df.shape[0],
        desc="compound names",
    ):
        compound, created = get_or_create_compound(ccache, row.compound_id)
        if compound is None:
            continue

        # If this is a duplicate, does not override the preferred name,
        # but appends all the common names to the list nevertheless
        if not created:
            logging.debug(f"The compound {row.compound_id} is a duplicate")
        else:
            compound.preferred_name = row.preferred_name

        for name in row.synonyms:
            cname, _ = cname_model.objects.get_or_create(name=name,
                                                         enabled=True)
            compound.common_names.add(cname)
        compound.save()


def json_to_eqapi_reaction(
        ccache: CompoundCache,
        json_dict: dict
) -> Union[equilibrator_api.Reaction, None]:

    reaction_id = "kegg:" + json_dict["RID"]
    sparse = dict()
    for coeff, kegg_cid in json_dict["reaction"]:
        compound_id = "kegg:" + kegg_cid
        if coeff == 0:
            # if one of the stoichiometric coefficients is 0, that usually
            # suggests that the reaction is not an explicit one (i.e.
            # contains template compounds, such as starch in R01790).
            # We skip these reactions since they will not be chemically
            # balanced in our system, and we cannot estimate their dG'0
            # anyway.
            logging.debug(
                f"Skipping reaction {reaction_id}: one of the reactants has a "
                f"coefficient of 0")
            return None

        eqapi_compound = ccache.get_compound(compound_id)
        if eqapi_compound is None:
            logging.warning(
                f"Skipping reaction {reaction_id} since one of the reactants is "
                f"not found in equilibrator-cache ({compound_id})")
            return None

        sparse[eqapi_compound] = float(coeff)

    if sparse is None or len(sparse) == 0:
        return None

    eqapi_reaction = equilibrator_api.Reaction(sparse=sparse, rid=reaction_id)

    if not eqapi_reaction.is_balanced():
        return None

    return eqapi_reaction


def load_kegg_reactions(ccache: CompoundCache) -> None:
    reaction_model = apps.get_model('gibbs.StoredReaction')
    for rd in tqdm(KEGG_REACTIONS_JSON, desc="stored reactions"):
        eqapi_reaction = json_to_eqapi_reaction(ccache, rd)
        if eqapi_reaction is None:
            continue

        reaction_hash = eqapi_reaction.hash_md5()
        if not reaction_hash:
            continue
        rxn, created = reaction_model.objects.get_or_create(
            reaction_hash=reaction_hash
        )

        if created:
            rxn.accession = eqapi_reaction.rid
            rxn.link = reaction_model.get_hyper_link(eqapi_reaction)
            rxn.reaction_string = reaction_model.get_reaction_string(
                eqapi_reaction
            )
            rxn.save()
        else:
            logging.debug(f"The reaction {rd['RID']} is a duplicate.")


def load_kegg_enzymes() -> None:
    enzyme_model = apps.get_model('gibbs.Enzyme')
    cname_model = apps.get_model('gibbs.CommonName')
    reaction_model = apps.get_model('gibbs.StoredReaction')

    for ed in tqdm(KEGG_ENZYMES_JSON, desc="enzymes"):
        ec = ed['EC']
        if not ec:
            logging.debug('Encountered an enzyme without an EC number.')
            continue

        names = ed['names']
        names.append(ec)  # add the EC number also as an optional name

        reactions = []
        for rid in ed['reaction_ids']:
            rxn = reaction_model.objects.filter(accession=f"kegg:{rid}")
            reactions += list(rxn)

        if not reactions:
            logging.debug('Ignoring EC %s: no reactions found' % ec)
            continue

        # Save first so we can do many-to-many mappings.
        try:
            enz, _ = enzyme_model.objects.get_or_create(ec=ec)
        except DataError as e:
            logging.warning(f"Ignoring EC {ec}: {e}")

        # Add names, reactions, and compound mappings.
        cnames = [cname_model.objects.get_or_create(name=n, enabled=True)[0]
                  for n in names]
        
        for n in cnames:
            enz.common_names.add(n)
        
        for r in reactions:
            enz.reactions.add(r)
        enz.save()


def load_additional_compound_data(ccache: CompoundCache) -> None:
    compound_model = apps.get_model('gibbs.Compound')
    cname_model = apps.get_model('gibbs.CommonName')

    for cd in tqdm(ADDITIONAL_DATA_JSON, desc="additional compound data"):
        compound_id = cd['compound_id']
        eqapi_compound = ccache.get_compound(compound_id)
        if eqapi_compound is None:
            logging.warning(
                f"Cannot find {compound_id} in equilibrator-cache"
            )
            continue

        compound = compound_model.objects.get(eqapi_id=eqapi_compound.id)
        if compound is None:
            logging.debug(
                f"Cannot find {compound} in the database."
            )
            continue

        if "note" in cd:
            compound.note = cd.get('note')
        if "preferred name" in cd:
            compound.preferred_name = cd.get('preferred name')
        if "details_link" in cd:
            compound.details_link = cd.get('details_link')
        if "names" in cd:
            cnames = [cname_model.objects.get_or_create(name=n, enabled=True)[0]
                      for n in cd["names"]]
            for n in cnames:
                compound.common_names.add(n)

        compound.save()
