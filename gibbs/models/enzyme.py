# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
from typing import List

from django.db import models

from .compound import CommonName
from .stored_reaction import StoredReaction


class Enzyme(models.Model):
    """A single enzyme."""

    # EC class enzyme.
    ec = models.CharField(max_length=20)

    # A list of common names of the enzyme, used for searching.
    common_names = models.ManyToManyField(CommonName)

    # List of reactions this enzyme catalyzes.
    reactions = models.ManyToManyField(StoredReaction)

    def HasData(self) -> bool:
        """Checks if it has enough data to display."""
        return self.ec and self.reactions.all()

    @property
    def link(self) -> str:
        """Returns a link to this reactions page."""
        return f"/enzyme?ec={self.ec}"

    @property
    def kegg_link(self) -> str:
        """Returns a link to the KEGG page for this enzyme."""
        return f"http://kegg.jp/dbget-bin/www_bget?ec:{self.ec}"

    @property
    def brenda_link(self) -> str:
        """Returns a link to the BRENDA page for this enzyme."""
        return f"http://www.brenda-enzymes.org/php/result_flat.php4?ecno={self.ec}"

    @property
    def all_reactions(self) -> List[StoredReaction]:
        """Returns all the reactions (unique by their 'hash')."""
        rxns = []
        hashes = set()
        for r in self.reactions.all():
            if r.reaction_hash not in hashes:
                rxns.append(r)
                hashes.add(r.reaction_hash)
        return rxns

    @property
    def first_name(self) -> "CommonName":
        """The first name in the list of names."""
        return self.all_common_names[0]

    def __str__(self) -> str:
        """Return a single string identifier of this enzyme."""
        return str(self.first_name.name)

    all_common_names = property(
        lambda self: sorted(self.common_names.all(), key=lambda n: n.id)
    )
    all_cofactors = property(lambda self: self.cofactors.all())
