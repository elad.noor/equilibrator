# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import logging
import urllib
from collections import defaultdict, namedtuple
from itertools import count
from typing import Callable, Dict, Iterable, List, Optional, Tuple, Union

from django.apps import apps
from django.db import models
from django.utils.text import slugify
from equilibrator_cache import PROTON_INCHI
from equilibrator_api import FARADAY, Q_, ComponentContribution
from numpy import abs, exp, floor, log, sign
from pint import Measurement

from util import constants

from .. import _ccache, _predictor, eqapi_Compound, eqapi_Reaction
from ..conditions import AqueousParams
from ..forms import ReactionForm

# proton
PROTON_FORMATION = eqapi_Reaction.parse_formula(
    _ccache.get_compound,
    "= kegg:C00080",
)

# H2O
WATER_FORMATION = eqapi_Reaction.parse_formula(
    _ccache.get_compound,
    "= kegg:C00001",
)

# ATP hydrolysis (to ADP)
ATP_REACTION = eqapi_Reaction.parse_formula(
    _ccache.get_compound,
    "kegg:C00002 + kegg:C00001 = kegg:C00008 + kegg:C00009",
)

# CO2 hydriolyis (to bicarbonate)
CO2_REACTION = eqapi_Reaction.parse_formula(
    _ccache.get_compound,
    "kegg:C00011 + kegg:C00001 = kegg:C01353",
)

_compound_names_and_accessions = [
    ("h2o", "metanetx.chemical:MNXM2"),
    ("nad", "metanetx.chemical:MNXM8"),
    ("nadh", "metanetx.chemical:MNXM10"),
    ("pi", "metanetx.chemical:MNXM9"),
    ("coa", "metanetx.chemical:MNXM12"),
    ("fe_2", "metanetx.chemical:MNXM111"),
    ("fe_3", "metanetx.chemical:MNXM196"),
    ("co2", "metanetx.chemical:MNXM13"),
    ("bicarbonate", "metanetx.chemical:MNXM60"),
]

COMP_DICT = {
    name: _ccache.get_compound(accession)
    for name, accession in _compound_names_and_accessions
}

ReactantCoeffName = namedtuple("ReactantCoeffName", "compound coeff name")


class ReactantFormulaMissingError(Exception):
    def __init__(self, c):
        self.compound = c

    def __str__(self):
        return (
            "Cannot test reaction balancing because the reactant"
            " %d does not have a chemical formula" % self.compound.eqapi_id
        )


class Reaction(models.Model):
    """A reaction."""

    def __init__(
        self,
        eqapi_reaction: eqapi_Reaction,
        compound_names: Dict[int, str] = None,
        aq_params: AqueousParams = None,
    ):
        """Construction.

        :param eqapi_reaction: a Reaction object.
        :param compound_names: a dictionary from compounds to the names
        that is to be used to display it.
        :param aq_params: aqueous parameters (such as pH, I, T, etc.)
        """
        super(Reaction, self).__init__()

        self._eqapi_reaction = eqapi_reaction
        # find all the Compound objects corresponding to the eqapi_compounds
        # and keep them in a dictionary
        self.compound_names = compound_names or dict()

        # TODO: consider merging between AqueousParams and ComponentContribution
        #  as a single class (they are quite redundant at the moment)
        self.comp_contrib = ComponentContribution(ccache=_ccache, predictor=_predictor)
        self.aq_params = aq_params or AqueousParams()

        # the following are only used as cache, no need to copy while cloning
        self._catalyzing_enzymes_cache = None

    @property
    def aq_params(self) -> AqueousParams:
        return self._aq_params

    @aq_params.setter
    def aq_params(self, aq_params: AqueousParams):
        """Change the aqueous parameters."""
        self._thermo_value_cache = defaultdict(str)  # reset thermodynamic values
        self._aq_params = aq_params
        aq_params.set_component_contribution(self.comp_contrib)

    @property
    def __len__(self):
        return len(self._eqapi_reaction)

    def Clone(self) -> "Reaction":
        """
        Make a copy of the reaction
        """
        logging.debug("Cloning reaction...")
        return Reaction(
            self._eqapi_reaction.clone(), self.compound_names.copy(), self.aq_params
        )

    @staticmethod
    def FromForm(
        form: ReactionForm, aq_params: Optional[AqueousParams] = None
    ) -> "Reaction":
        """
        Build a reaction object from a ReactionForm.

        Args:
            form :
                a ReactionForm object.
            aq_params : AqueousParams, optional
                The Aqueous environment parameters.

        Returns:
            A Reaction object or None if there's an error.
        """

        compounds = []
        for cid in form.cleaned_reactantsId:
            compound = _ccache.get_compound_by_internal_id(cid)
            if compound is None:
                raise KeyError(f"Could not find a compound with this ID: {cid}")
            compounds.append(compound)
        coefficients = list(form.cleaned_reactantsCoeff)
        reactant_ids = list(form.cleaned_reactantsId)
        reactant_names = list(form.cleaned_reactantsName)
        phases = list(form.cleaned_reactantsPhase)
        abundances = list(form.cleaned_reactantsAbundance)
        abundance_units = list(form.cleaned_reactantsAbundanceUnit)

        if len(compounds) > 1:
            # drop all instances of H+ from the reaction (we use Alberty's framework
            # where protons are buffered in a solution with constant pH).
            # unless H+ is the only compound in this reaction (so we want
            # to present the "formation" page for H+).
            for i, cpd in enumerate(compounds):
                if _ccache.is_proton(cpd):
                    compounds.pop(i)
                    reactant_ids.pop(i)
                    reactant_names.pop(i)
                    coefficients.pop(i)
                    if phases:
                        phases.pop(i)
                        abundances.pop(i)
                        abundance_units.pop(i)

        eqapi_reaction = eqapi_Reaction(
            dict(zip(compounds, coefficients))
        )

        if not phases:
            for cpd in compounds:
                phased_compound, _ = eqapi_reaction.get_phased_compound(cpd)
                phased_compound.abundance = constants.DEFAULT_ABUNDANCE[
                    phased_compound.phase
                ]
        else:
            for cpd, phase, abundance, unit in zip(
                compounds,
                phases,
                abundances,
                abundance_units,
            ):
                phased_compound, _ = eqapi_reaction.get_phased_compound(cpd)

                if _ccache.is_proton(cpd) or _ccache.is_water(cpd):
                    # avoid changing the phase or abundance of H+ or water
                    continue

                if phase != "None" and phase != phased_compound.phase:
                    # change the phase, in this case we also reset the abundance to its
                    # default value in the new phase
                    phased_compound.phase = phase

                if abundance != "None":
                    # set the new abundance to the value given in the form
                    new_abundance = Q_(float(abundance), unit)
                else:
                    # if a new abundance is not given use the default value in the
                    # current phase
                    new_abundance = constants.DEFAULT_ABUNDANCE[
                        phased_compound.phase
                    ]

                if new_abundance and not new_abundance.check(
                    phased_compound.condition.dimensionality
                ):
                    # if the given value doesn't match in terms of units (e.g. if the
                    # phased has changed but the units are still not updated), we reset
                    # the abundance to the default value in the current phase
                    new_abundance = constants.DEFAULT_ABUNDANCE[
                        phased_compound.phase
                    ]

                phased_compound.abundance = new_abundance

        compound_names = dict(zip(reactant_ids, reactant_names))
        return Reaction(eqapi_reaction, compound_names, aq_params=aq_params)

    @staticmethod
    def FromIds(
        compound_list: List[Dict[str, str]], aq_params: Optional[AqueousParams] = None
    ):
        """Build a reaction object from a list of compound dictionaries.

        Args:
            compound_list : list
                an iterable of dictionaries of reactants.
            aq_params : AqueousParams, optional
                The Aqueous environment parameters.

        Returns:
            a properly set-up Reaction object or None if there's an error.
        """
        compound_ids = [int(d["id"]) for d in compound_list]
        coefficients = [float(d["coeff"]) for d in compound_list]
        names = [d["name"] for d in compound_list]
        phases = [d["phase"] for d in compound_list]

        compounds = list(map(_ccache.get_compound_by_internal_id, compound_ids))

        for i, cpd in enumerate(compounds):
            if _ccache.is_proton(cpd):
                compounds.pop(i)
                compound_ids.pop(i)
                coefficients.pop(i)
                names.pop(i)
                phases.pop(i)

        eqapi_reaction = eqapi_Reaction(dict(zip(compounds, coefficients)))
        for cpd, phase in zip(compounds, phases):
            if phase is not None:
                eqapi_reaction.set_phase(cpd, phase)
            phased_compound, _ = eqapi_reaction.get_phased_compound(cpd)
            eqapi_reaction.set_abundance(
                cpd, constants.DEFAULT_ABUNDANCE[phased_compound.phase]
            )

        compound_names = dict(zip(compound_ids, names))
        return Reaction(eqapi_reaction, compound_names, aq_params=aq_params)

    def Reverse(self) -> "Reaction":
        """Swap the sides of this reaction."""
        logging.debug("Reversing reaction...")
        return Reaction(
            self._eqapi_reaction.reverse(), self.compound_names.copy(), self.aq_params
        )

    def get_compound_name(self, eqapi_cpd: eqapi_Compound) -> str:
        if eqapi_cpd.id not in self.compound_names:
            compounds = apps.get_model("gibbs.Compound").objects.filter(
                eqapi_id=eqapi_cpd.id
            )
            assert len(compounds) == 1, f"Could not find {eqapi_cpd} in the database"
            self.compound_names[eqapi_cpd.id] = compounds[0].FirstName()
        return self.compound_names[eqapi_cpd.id]

    def items(self) -> Iterable[Tuple[eqapi_Compound, float]]:
        """Iterate all equilibrator-api reactants."""
        return self._eqapi_reaction.items()

    def _get_one_side_reactant_dicts(
        self, filter_coeff_by: Callable[[float], bool]
    ) -> List[dict]:
        """Return a list of dictionaries with reactant info.

        Parameters
        ----------
        filter_coeff_by : Callable
            a function that filters the stoichiometric
            coefficients. For example, if can select all negative values to get
            the list of all substrate data.

        Returns
        -------
        reactant_dicts : List[dict]
            a list of dictionaries with reactant info.
        """
        reactant_dicts = []
        for compound, coeff in self.items():
            name = self.get_compound_name(compound)
            if filter_coeff_by(coeff):
                if abs(coeff).round(3) != 1:
                    coeff_str = f"{abs(coeff):.3g}"
                else:
                    coeff_str = ""

                phased_cpd, _ = self._eqapi_reaction.get_phased_compound(compound)
                if phased_cpd is not None:
                    phase_shorthand = phased_cpd.phase_shorthand
                else:
                    phase_shorthand = ""

                reactant_dicts.append(
                    {
                        "name": f"{name}{phase_shorthand}",
                        "href": f"/metabolite?compoundId="
                        f"{phased_cpd.id}&reactantsPhase="
                        f"{phased_cpd.phase}&reactantsAbundance=1.000&reactantsAbundanceUnit"
                        f"=millimolar",
                        "title": compound.formula,
                        "coeff": coeff_str,
                    }
                )

        return reactant_dicts

    @property
    def substrates(self) -> List[dict]:
        return self._get_one_side_reactant_dicts(lambda c: c < 0)

    @property
    def products(self) -> List[dict]:
        return self._get_one_side_reactant_dicts(lambda c: c > 0)

    def __str__(self) -> str:
        """
        Simple text reaction representation.
        """
        return self.GetQueryString()

    def GetCompoundData(self) -> List[dict]:
        return self._eqapi_reaction.serialize()

    def hash_md5(self) -> str:
        if self._eqapi_reaction:
            return self._eqapi_reaction.hash_md5()
        else:
            return ""

    @property
    def special_reaction_warning(self) -> Union[str, bool]:
        def GetLearnMoreLink(faq_mark) -> str:
            return (
                f'</br><a href="/static/classic_rxns/faq.html#{faq_mark}">'
                "Learn more &raquo;</a>"
            )

        compound_list = list(self._eqapi_reaction.keys())

        if self._eqapi_reaction == PROTON_FORMATION:
            return (
                "The formation energy of a proton is defined as 0."
                + GetLearnMoreLink("why-can-t-i-change-the-concentration-of-h-ions")
            )
        elif self._eqapi_reaction == WATER_FORMATION:
            return (
                    "We assume that the concentration of water is fixed in aqueous "
                    "environments." + GetLearnMoreLink(
                    "why-can-t-i-change-the-concentration-of-water"
                )
            )
        elif self._eqapi_reaction == ATP_REACTION:
            return (
                "The &Delta;G' of ATP hydrolysis is highly affected "
                + "by Mg<sup>2+</sup> ions."
                + GetLearnMoreLink("atp-hydrolysis")
            )
        elif self._eqapi_reaction == CO2_REACTION:
            return (
                "You are looking at the &Delta;G' of CO<sub>2</sub> hydration."
                + GetLearnMoreLink("co2-total")
            )
        elif COMP_DICT["fe_2"] in compound_list and COMP_DICT["fe_3"] in compound_list:
            return (
                "Energetics of iron redox reactions depend heavily on the "
                + "chemical forms of iron involved."
                + GetLearnMoreLink("iron-redox")
            )
        elif (
            COMP_DICT["co2"] in compound_list
            and COMP_DICT["bicarbonate"] in compound_list
        ):
            return (
                "One should not use CO<sub>2</sub>(aq) together with "
                + "CO<sub>2</sub>(total) in the same reaction."
                + GetLearnMoreLink("co2-total")
            )
        elif (
            COMP_DICT["co2"] in compound_list
            and self.get_replace_co2_link() is not None
        ):
            return (
                'Did you mean <a href="%s">CO<sub>2</sub>(total)</a>?'
                % self.get_replace_co2_link()
                + GetLearnMoreLink("co2-total")
            )
        else:
            return False

    def _GetAllStoredReactions(self) -> list:
        """Find all stored reactions matching this reaction (using the hash)."""
        reaction_hash = self.hash_md5()
        if not reaction_hash:
            return []
        return apps.get_model("gibbs.StoredReaction").objects.filter(
            reaction_hash=reaction_hash
        )

    @property
    def stored_reaction_id(self) -> str:
        """Find a reaction in MetaNetX that matches this one.

        :return: The MetaNetX reaction ID.
        """
        # TODO: we need to implement this search using the MetaNetX reaction
        # database (and add it to the equilibrator-api)
        return ""

    @property
    def enzyme_slug(self) -> Union[str, None]:
        if self.catalyzing_enzymes:
            enz = str(self.catalyzing_enzymes[0].first_name.name)
            enz_slug = slugify(enz)[:10]
            enz_slug = enz_slug.replace("-", "_")
            return enz_slug
        else:
            return None

    UNIQUE_REACTION_COUNTER = count()

    def GenerateUniqueName(self) -> str:
        """
        Generate a relatively short string that can be used as a unique
        ID in a model.
        """
        enz_slug = self.enzyme_slug or "RXN"
        unique_id = self.stored_reaction_id or "Q%05d" % next(
            Reaction.UNIQUE_REACTION_COUNTER
        )
        return "R_%s_%s" % (enz_slug, unique_id)

    @property
    def catalyzing_enzymes(self) -> list:
        """
        Get all the enzymes catalyzing this reaction.
        """
        if self._catalyzing_enzymes_cache is None:
            logging.debug("looking for enzymes catalyzing this reaction")
            self._catalyzing_enzymes_cache = set()
            for stored_reaction in self._GetAllStoredReactions():
                enzymes = stored_reaction.enzyme_set.all()
                self._catalyzing_enzymes_cache.update(enzymes)
        return list(self._catalyzing_enzymes_cache)

    def _GetAtomDiff(self) -> Dict[str, int]:
        """Returns the net atom counts from this reaction.

        :return: a dictionary from atoms to counts. None if a formula is
        missing.
        """
        return self._eqapi_reaction._get_reaction_atom_bag(minimal_stoichiometry=1e-2)

    def _GetUrlParams(
        self, query: Optional[str] = None, reset: bool = False
    ) -> List[str]:
        """Get the URL params for this reaction."""
        params = self.aq_params.get_url_params()
        for compound, coeff in self.items():
            name = self.get_compound_name(compound)
            phased_compound, _ = self._eqapi_reaction.get_phased_compound(compound)

            params.append(f"reactantsId={compound.id}")
            params.append(f"reactantsName={name}")
            params.append(f"reactantsCoeff={coeff:.3g}")
            params.append(f"reactantsPhase={phased_compound.phase or 'None'}")

            if reset:
                c = constants.DEFAULT_ABUNDANCE[phased_compound.phase]
            else:
                c = phased_compound.abundance
            abundance, abundance_unit = self.abundance_to_strings(c)
            params.append(f"reactantsAbundance={abundance}")
            params.append(f"reactantsAbundanceUnit={abundance_unit}")

        if query is not None:
            for arrow in constants.POSSIBLE_REACTION_ARROWS:
                # replace all possible arrows with =>
                tmp_query = query.replace(arrow, "=>")
            params.append(f"query={urllib.parse.quote_plus(tmp_query)}")

        return params

    def GetHyperlink(
        self, query: Optional[str] = None, reset: bool = False
    ) -> Optional[str]:
        params = self._GetUrlParams(query=query, reset=reset)
        params_str = "&".join(params)
        return f"/reaction?{params_str}"

    def GetResetLink(self, query: str = None) -> Optional[str]:
        return self.GetHyperlink(query=query, reset=True)

    def BalanceWithLink(self, compound, query=None) -> Optional[str]:
        """Returns a link to balance this reaction with a given compound."""
        other = self.TryBalancing(compound)
        if other is None:
            return None
        return other.GetHyperlink(query, reset=True)

    def GetBalanceElectronsLink(self, query=None) -> Optional[str]:
        """
        Returns a link to the same reaction,
        balanced by extra NAD+/NADH pairs.
        """
        other = self.BalanceElectrons()
        if other is None:
            return None
        return other.GetHyperlink(query, reset=True)

    def get_replace_co2_link(self, query=None) -> Optional[str]:
        """
        Returns a link to the same reaction, but with HCO3-
        instead of CO2.
        """
        rxn = self._replace_compound(COMP_DICT["co2"], COMP_DICT["bicarbonate"])
        rxn = rxn.TryBalancing(COMP_DICT["h2o"])
        if rxn is None:
            return None
        return rxn.GetHyperlink(query, reset=True)

    @staticmethod
    def abundance_to_strings(
        abundance: Q_,
    ) -> Tuple[str, str]:
        if abundance is None:
            return "None", "None"
        if abundance.check("[concentration]"):
            basic_unit = "molar"
        elif abundance.check("[pressure]"):
            basic_unit = "bar"
        else:
            # not molar or bar units, just print whatever `pint` thinks is best
            return f"{abundance.magnitude:.3f}", str(abundance.units)

        for u in [prefix + basic_unit for prefix in ["", "milli", "micro", "nano"]]:
            if abundance > Q_(0.1, u):
                return abundance.m_as(u), u
        # out of range, just print whatever `pint` thinks is best
        return f"{abundance.magnitude:.3f}", str(abundance.units)

    def get_template_data(self, query=None) -> dict:
        # generate a list for the reactant whose concentrations can be
        # set by the user
        reactant_data = []
        for compound, coeff in self.items():
            name = self.get_compound_name(compound)
            phased_compound, _ = self._eqapi_reaction.get_phased_compound(compound)
            rdata = {
                "coeff": coeff,
                "compound_id": compound.id,
                "name": name,
                "phase": phased_compound.phase,
                "possible_phases": phased_compound.possible_phases,
            }

            abundance, abundance_unit = Reaction.abundance_to_strings(
                phased_compound.abundance
            )
            rdata.update(
                {
                    "abundance": abundance,
                    "abundance_unit": abundance_unit,
                }
            )
            reactant_data.append(rdata)

        template_data = {
            "reaction": self,
            "reactant_data": reactant_data,
            "query": query,
            "balance_with_water_link": self.BalanceWithLink(COMP_DICT["h2o"], query),
            "balance_with_coa_link": self.BalanceWithLink(COMP_DICT["coa"], query),
            "balance_with_pi_link": self.BalanceWithLink(COMP_DICT["pi"], query),
            "balance_electrons_link": self.GetBalanceElectronsLink(query),
        }

        template_data.update(self.aq_params.get_template_data())
        return template_data

    def GetPhGraphLink(self) -> str:
        params = self._GetUrlParams()
        params.append("vary_ph=1")
        return "/graph_reaction?%s" % "&".join(params)

    def GetPMgGraphLink(self) -> str:
        params = self._GetUrlParams()
        params.append("vary_pmg=1")
        return "/graph_reaction?%s" % "&".join(params)

    def GetIonicStrengthGraphLink(self) -> str:
        params = self._GetUrlParams()
        params.append("vary_is=1")
        return "/graph_reaction?%s" % "&".join(params)

    def GetQueryString(self, slufigy=False) -> str:
        """Get a query string for this reaction."""
        rdict = {-1: [], 1: []}
        for compound, coeff in self.items():
            name = self.get_compound_name(compound)
            abs_coeff = abs(coeff)
            if slufigy:
                name = slugify(name)
            s = sign(coeff)
            if s == 0:
                continue
            if abs_coeff == 1:
                rdict[s].append(name)
            else:
                rdict[s].append("%g %s" % (abs_coeff, name))

        return "%s <=> %s" % (" + ".join(rdict[-1]), " + ".join(rdict[1]))

    @property
    def is_reactant_formula_missing(self) -> bool:
        return self._GetAtomDiff() is None

    @property
    def reactants_with_missing_formula(self) -> Iterable[str]:
        for compound in self._eqapi_reaction.keys():
            if compound.atom_bag is None:
                yield str(compound)

    @property
    def is_empty(self) -> bool:
        if not self._eqapi_reaction:
            return True
        else:
            return self._eqapi_reaction.is_empty()

    @property
    def is_balanced(self) -> bool:
        """Checks if the collection is atom-wise balanced.

        :return: True if the collection is atom-wise balanced.
        """
        return self._eqapi_reaction.is_balanced(
            ignore_atoms=("H",),
        )

    @property
    def is_half_reaction(self) -> bool:
        """Checks if the collection is electron-wise balanced.

        :return: True if the collection is a half-reaction. Note that the
        function will return False if the reaction is not atom-balanced,
        has no formula, or if it is completely balanced including electrons.
        """
        if self.is_balanced:
            return False
        else:
            return self._eqapi_reaction.is_balanced(
                ignore_atoms=(
                    "H",
                    "e-",
                ),
            )

    def _replace_compound(
        self, old_compound: eqapi_Compound, new_compound: eqapi_Compound
    ) -> "Reaction":
        """Replace one compound with another

        :return: The new reaction.
        """
        if old_compound not in self._eqapi_reaction.sparse:
            return self.Clone()

        new_sparse = dict(self._eqapi_reaction.sparse)
        new_sparse[new_compound] = new_sparse[old_compound]
        del new_sparse[old_compound]

        return Reaction(
            eqapi_reaction=eqapi_Reaction(new_sparse),
            compound_names=self.compound_names.copy(),
            aq_params=self.aq_params,
        )

    def TryBalancing(self, compound: eqapi_Compound) -> "Reaction":
        """Try to balance the reaction with a given compound.

        :return: If the reaction can be balanced, return the
        balanced reaction. Otherwise return None.
        """
        new_eqapi_reaction = self._eqapi_reaction.balance_with_compound(
            compound=compound
        )
        if new_eqapi_reaction is None:
            return None

        return Reaction(
            new_eqapi_reaction, self.compound_names, aq_params=self.aq_params
        )

    def AddCompound(self, compound: eqapi_Compound, coeff: float) -> None:
        """Adds "how_many" of the compound with the given id.

        Args:
            compound: the Compound object.
            coeff: by how much to change the reactant's coefficient.
        """
        self._eqapi_reaction.add_stoichiometry(compound, coeff)

    def _GetElectronDiff(self) -> int:
        atom_bag = self._GetAtomDiff()
        net_electrons = atom_bag.get("e-", 0)
        return net_electrons

    def BalanceElectrons(
        self, acceptor=COMP_DICT["nad"], reduced_acceptor=COMP_DICT["nadh"], n_e=2
    ) -> "Reaction":
        """Try to balance the reaction electons.

        By default acceptor and reduced accepter differ by 2e-.
        """
        net_electrons = self._GetElectronDiff()
        if net_electrons == 0:
            return self

        rxn = self.Clone()
        rxn.AddCompound(acceptor, net_electrons / float(n_e))
        rxn.AddCompound(reduced_acceptor, -net_electrons / float(n_e))
        return rxn

    def _GetThermoValue(self, cc_method: str) -> Measurement:
        """Calculate a DeltaG using ComponentContribution.

        :param cc_method: The name of the ComponentContribution method to use
        for the calculation
        :return: A tuple (float, float) of the DeltaG and stderr (in kJ/mol)
        """
        dg_func = self.comp_contrib.__getattribute__(cc_method)
        return dg_func(self._eqapi_reaction)

    def _GetThermoValueString(self, cc_method: str) -> str:
        """Calculate a DeltaG value using ComponentContribution.

        First checks the cache (_thermo_values) to see if this value has been
        calculated. Otherwise, uses ComponentContribution to calculate it and
        stores the result.

        :param cc_method: The name of the ComponentContribution method to use
        for the calculation
        """
        if cc_method not in self._thermo_value_cache:
            tvalue = self._GetThermoValue(cc_method)

            if tvalue.check("[energy]/[substance]"):
                if tvalue.error.m_as("kJ/mol") > 1000:
                    self._thermo_value_cache[cc_method] = "<unknown>"
                else:
                    dg = tvalue.value.m_as("kJ/mol")
                    s_dg = tvalue.error.m_as("kJ/mol")
                    self._thermo_value_cache[cc_method] = (
                        f"<strong>{dg:.1f}</strong> &plusmn; "
                        f"{1.96*s_dg:.1f} [kJ/mol]"
                    )
            elif tvalue.check("volt"):
                if tvalue.error.m_as("V") > 1:
                    self._thermo_value_cache[cc_method] = "<unknown>"
                else:
                    e = tvalue.value.m_as("volt")
                    s_e = 1.96 * tvalue.error.m_as("volt")
                    self._thermo_value_cache[
                        cc_method
                    ] = f"<strong>{e:.3f}</strong> &plusmn; {s_e:.3f} [V]"
            else:
                raise Exception(
                    f"Thermodynamic values must be in kJ/mol or "
                    f"in volts, given: {tvalue}"
                )

        return self._thermo_value_cache[cc_method]

    @property
    def half_reaction_dg_prime(self) -> str:
        """Compute the deltaG' for a half-reaction.

        Assuming the missing electrons are provided in a certain potential
        (e_potential)

        Returns:
            The deltaG' for this half-reaction, or None if data was missing.
        """
        if "half_reaction_dg_prime" not in self._thermo_value_cache:
            standard_dg_prime = self._GetThermoValue("standard_dg_prime")

            if standard_dg_prime.error.m_as("kJ/mol") > 1000:
                self._thermo_value_cache["half_reaction_dg_prime"] = ""
            else:
                dg_prime = (
                    standard_dg_prime
                    + FARADAY * self.aq_params.e_potential * self._GetElectronDiff()
                )
                dg_prime_in_kj_per_mol = dg_prime.value.m_as("kJ/mol")
                dg_confidence_interval = 1.96 * dg_prime.error.m_as("kJ/mol")

                self._thermo_value_cache["half_reaction_dg_prime"] = (
                    f"<strong>{dg_prime_in_kj_per_mol:.1f}</strong> &plusmn; "
                    f"{dg_confidence_interval:.1f} [kJ/mol]"
                )

        return self._thermo_value_cache["half_reaction_dg_prime"]

    @property
    def k_eq_prime(self) -> str:
        """Returns the transformed equilibrium constant for this reaction.

        In a human readable formet (using HTML superscript).
        """
        if "keq_prime" not in self._thermo_value_cache:
            standard_dg_prime = self._GetThermoValue("standard_dg_prime")

            if standard_dg_prime.error.m_as("kJ/mol") > 1000:
                self._thermo_value_cache["keq_prime"] = ""

            dg_over_rt = -(standard_dg_prime.value / self.comp_contrib.RT).m_as("")

            k_eq = exp(dg_over_rt)

            if 1e-2 <= k_eq <= 1e2:
                self._thermo_value_cache["keq_prime"] = f"{k_eq:.1g}"
            else:
                x = dg_over_rt / log(10)
                expo = int(floor(x))
                prefactor = 10 ** (x - expo)
                self._thermo_value_cache[
                    "keq_prime"
                ] = f"{prefactor:.1f} &times; 10<sup>{expo}</sup>"

        return self._thermo_value_cache["keq_prime"]

    def NoDeltaGExplanation(self) -> Union[str, None]:
        """Get an explanation for why there's no delta G value.

        :return: The explanation or an empty string.
        """
        for eqapi_cpd, _ in self.items():
            compounds = apps.get_model("gibbs.Compound").objects.filter(
                eqapi_id=eqapi_cpd.id
            )
            if len(compounds) != 1:
                continue
            if compounds[0].no_dg_explanation:
                name = compounds[0].FirstName()
                return f"{name} {compounds[0].no_dg_explanation.lower()}"
        return "the uncertainty is too high"

    @staticmethod
    def unbalanced_to_html(atom_bag: List[Tuple[str, int]], electrons: int) -> str:
        if len(atom_bag) == 0 and electrons > 0:
            return f"{electrons:g}e<sup>-</sup>"
        if (
            len(atom_bag) == 1
            and atom_bag[0][0] == "O"
            and electrons == 10 * atom_bag[0][1]
        ):
            # if oxygen and electrons are missing with a ratio of 1 to 10
            # it is an indication that water molecules have been neglected
            return f"{atom_bag[0][1]:g} H<sub>2</sub>O"
        return "".join(
            [
                atom + ("" if coefficient == 1 else f"<sub>{coefficient:g}</sub>")
                for atom, coefficient in atom_bag
            ]
        )

    @property
    def extra_atoms(self) -> Optional[str]:
        try:
            diff = self._GetAtomDiff()
        except ReactantFormulaMissingError:
            return None
        diff.pop("H", 0)
        extra_electrons = max(0, -diff.pop("e-", 0))
        extra_atoms = sorted(
            [
                (atom, -coefficient)
                for atom, coefficient in diff.items()
                if coefficient < 0
            ]
        )
        return Reaction.unbalanced_to_html(extra_atoms, extra_electrons)

    @property
    def missing_atoms(self) -> Optional[str]:
        try:
            diff = self._GetAtomDiff()
        except ReactantFormulaMissingError:
            return None
        diff.pop("H", 0)
        missing_electrons = max(0, diff.pop("e-", 0))
        missing_atoms = sorted(
            [
                (atom, coefficient)
                for atom, coefficient in diff.items()
                if coefficient > 0
            ]
        )
        return Reaction.unbalanced_to_html(missing_atoms, missing_electrons)

    @property
    def extra_electrons(self) -> int:
        try:
            diff = self._GetAtomDiff()
        except ReactantFormulaMissingError:
            return 0
        return max(0, -diff.get("e-", 0))

    @property
    def missing_electrons(self) -> int:
        try:
            diff = self._GetAtomDiff()
        except ReactantFormulaMissingError:
            return 0
        return max(0, diff.get("e-", 0))

    @property
    def dg_analysis(self):
        return self.comp_contrib.dg_analysis(self._eqapi_reaction)

    @property
    def standard_dg(self) -> str:
        return self._GetThermoValueString("standard_dg")

    @property
    def standard_dg_prime(self) -> str:
        return self._GetThermoValueString("standard_dg_prime")

    @property
    def physiological_dg_prime(self) -> str:
        return self._GetThermoValueString("physiological_dg_prime")

    @property
    def dg_prime(self) -> str:
        return self._GetThermoValueString("dg_prime")

    @property
    def standard_e_prime(self) -> str:
        return self._GetThermoValueString("standard_e_prime")

    @property
    def physiological_e_prime(self) -> str:
        return self._GetThermoValueString("physiological_e_prime")

    @property
    def e_prime(self) -> str:
        return self._GetThermoValueString("e_prime")

    @property
    def is_physiological(self) -> bool:
        if self._eqapi_reaction is None:
            return False
        else:
            for cpd, _ in self.items():
                if self._eqapi_reaction.get_abundance(cpd) not in [
                    None,
                    Q_("1 mM"),
                    Q_("1 mbar"),
                ]:
                    return False
            return True

    no_dg_explanation = property(NoDeltaGExplanation)
    link_url = property(GetHyperlink)
    ph_graph_link = property(GetPhGraphLink)
    pmg_graph_link = property(GetPMgGraphLink)
    is_graph_link = property(GetIonicStrengthGraphLink)
    reset_link = property(GetResetLink)
