# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import json

from django.db import models


class ValueSource(models.Model):
    """
    The source of a particular numeric value.
    """

    # A JSON of all the relevant data.
    name = models.CharField(max_length=30)

    # A JSON of all the relevant data.
    data = models.CharField(max_length=2048)

    def __init__(self, *args, **kwargs):
        super(ValueSource, self).__init__(*args, **kwargs)
        self._data_dict = json.loads(self.data)

    def __unicode__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)

    def __str__(self):
        return self.name

    def __eq__(self, other):
        """Equality operator."""
        if not other:
            return False

        if not hasattr(other, "name"):
            return False

        return self.name == other.name

    def GetAuthorList(self):
        return ", ".join(self._data_dict.get("author", []))

    author = property(GetAuthorList)

    def GetYear(self):
        return self._data_dict.get("year", "2000 BC")

    year = property(GetYear)

    def GetTitle(self):
        return self._data_dict.get("title", None)

    title = property(GetTitle)

    def GetJournal(self):
        return self._data_dict.get("journal", "")

    journal = property(GetJournal)

    def GetURL(self):
        return self._data_dict.get("url", None)

    url = property(GetURL)

    def GetDOI(self):
        return self._data_dict.get("doi", None)

    doi = property(GetDOI)

    def GetPMID(self):
        return self._data_dict.get("pmid", None)

    pmid = property(GetPMID)

    def GetCitation(self):
        if not self.title:
            return ""
        if self.url is not None:
            return '%s. <a href="%s"><strong>%s</strong></a>. <i>%s</i>' % (
                self.author,
                self.url,
                self.title,
                self.journal,
            )
        else:
            return "%s. <strong>%s</strong>. <i>%s</i>" % (
                self.author,
                self.title,
                self.journal,
            )

    citation = property(GetCitation)
