# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import logging
import os

from component_contribution.predict import GibbsEnergyPredictor
from equilibrator_api import Q_
from equilibrator_api import Reaction as eqapi_Reaction
from equilibrator_cache import Compound as eqapi_Compound
from equilibrator_cache.compound_cache import CompoundCache
from sqlalchemy import create_engine

logging.getLogger().setLevel(logging.INFO)

db_user = os.environ.get("PGUSER", "eqbtr_user")
db_host = os.environ.get("PGHOST", "localhost")
cpd_db_name = "compounds"

logging.info("Connection to compound cache stored on PostgreSQL.")
_ccache = CompoundCache(
    create_engine(f"postgresql://{db_user}@{db_host}/{cpd_db_name}")
)

logging.info("Initializing the component contribution module.")
_predictor = GibbsEnergyPredictor(rmse_inf=Q_("1e5 kJ/mol"))
